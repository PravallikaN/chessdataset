#!/usr/bin/env python
# coding: utf-8

from PIL import Image
import os

f = r'D:\3rd Year\ELITE_IMAGE_PROCESSING\TEST_IMAGES'

def removeBlackBgInKing(img):
    img = img.convert("RGB")
    datas = img.getdata()
    new_image_data = []
    for item in datas:
        if item[0] == 0:
            new_image_data.append((255, 255, 255))
        else:
            new_image_data.append(item)
    img.putdata(new_image_data)
    return img

for file in os.listdir(f):
    f_img = f+"/"+file
    img = Image.open(f_img)
    img = img.rotate(5) #For rotating image by angle of 5
    img = img.convert("L") # convert image to monochrome
    img = img.crop(box = (415,90,585,450)) # crop image
    img = removeBlackInKing(img) # for removing black background when the image is rotated
    img.thumbnail((224,224))
    img.save(f_img)






