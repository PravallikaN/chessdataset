import os
from PIL import Image

def compressImages(i, o):
    ip = r"" + i
    for image in os.listdir(ip):
        img = Image.open(ip + os.sep + image)
        out_path = r"" + o + os.sep + "compressed_" + image.replace("-","_") 
        img.save(out_path, optimize = True, quality = 18)
        img = Image.open(out_path)
    return "Images in " + i + " directory are compressed and stored in " + o + " directory"

in_path = input("Enter input directory name : ")
out_path = input("Enter output directory name : ")
print(compressImages(in_path, out_path))
