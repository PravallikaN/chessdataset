from PIL import Image, ImageFilter, ImageOps
from random import choice, randint
import os

def grayscale(img):
    return img.convert("L")

def blur(img):
    return img.filter(ImageFilter.BoxBlur(randint(1, 8)))

def equalize(img):
    return ImageOps.equalize(img.convert('RGB'), mask = None)

transformations = [grayscale, blur, equalize]

folder = r"" + input()
for file in os.listdir(folder):
    path = folder + "/" + file
    img = Image.open(path)
    img1 = choice(transformations)(img)
    img1.save(path)
