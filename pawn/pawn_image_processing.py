from PIL import Image, ImageFilter
from random import randint
from PIL import Image
import os

f = input()
folder = r"" + f

def cropAndRotate(img):
    width, height = img.size
    img = img.crop((width / 4, height / 100, width, height))
    img = img.convert('RGBA')
    rot = img.rotate(randint(-15, 15), expand = 1)
    return rot

def masking(img, width, height):
    msk = Image.new('RGBA', (width, height), (255,) * 4)
    msk_img = Image.composite(img, msk, img)
    msk_img.convert(img.mode)
    return msk_img

def convertToSqr(img, bg):
    width, height = img.size
    if width == height:
        return img
    if width > height:
        sqrImg = Image.new(img.mode, (width, width), bg)
        sqrImg.paste(img, (0, (width - height) // 2))
        return sqrImg
    sqrImg = Image.new(img.mode, (height, height), bg)
    sqrImg.paste(img, ((height - width) // 2, 0))
    return sqrImg
        
for file in os.listdir(folder):
    path = folder + "/" + file
    img = Image.open(path)
    
    if "Color_0" in f:
        width, height = img.size  
        img = img.crop((width / 4, height / 4, width, 3.5 * height / 4))

    rot = cropAndRotate(img)
    width, height = rot.size
    msk_img = masking(rot, width, height)
    
    width, height = msk_img.size
    msk_img = msk_img.crop((400, 200, 1200, 1200))
    
    img1 = msk_img.convert('RGBA')
    msk_img1 = masking(img1, 800, 1200)
    msk_img1.thumbnail((224, 224))
    
    sqr_img = convertToSqr(msk_img1, (255, 255, 255))
    sqr_img.save(path)
