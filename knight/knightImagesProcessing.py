import os
from PIL import Image 
import random

SRC = "/home/mamatha/Desktop/ELITE_WORK/sampleData/" 
DES = "/home/mamatha/Desktop/ELITE_WORK/Knight/"

def transparentToWhite(img):
    img = img.convert("RGB")
    datas = img.getdata()
    new_image_data = []
    for item in datas:
        if item[0] == 0:
            new_image_data.append((255, 255, 255))
        else:
            new_image_data.append(item)
    img.putdata(new_image_data)
    return img

sizesDict = {'0': [[350,50,760,460],[300,80,710,490],[350, 80, 760, 490],[260, 80, 670, 490],[260,60,670,470],[260,80,670,490]], 
'15': [[350,70,760,480],[350,70,760,480],[270, 80, 680, 490],[350,80,760,490],[260,70,670,480],[240,80,650,490]], 
'30':[[350,70,760,480],[350,70,760,480],[270, 80, 680, 490],[350,80,760,490],[260,70,670,480],[240,80,650,490]], 
'45':[[350, 100, 760, 510], [300, 90, 710, 500], [350, 110, 760, 520], [360, 100, 770, 510], [290, 90, 700, 500], [330, 90, 740, 500]],'60':[[350, 120, 760, 530], [300, 130, 710, 540], [350, 110, 760, 520], [290, 120, 700, 530], [390, 120, 800, 530]], 
'75':[[350,120,760,530],[330,130,740,540],[350,110,760,520],[460,120,870,530],[390,160,800,570]]}

for folder in (os.listdir(SRC)):
    if folder.split("_")[1] == "Knight" and folder.split("_")[3] in sizesDict.keys():
        
        folder = SRC + folder + '/'
        sizesList = sizesDict[folder.split("_")[5]]
            
        for im in (os.listdir(folder)):
        
            img = Image.open(SRC + im)
            resizedImg = img.resize((round(img.size[0]*0.5), round(img.size[1]*0.5)),Image.ANTIALIAS)

            deg = random.randint(-20,21)
            print(deg)
            rotatedImg = resizedImg.rotate(deg, Image.BICUBIC)

            sizes = sizesList[random.randint(0,4)]
            print(sizes)
            croppedImg = rotatedImg.crop((sizes[0], sizes[1], sizes[2], sizes[3]))
    
            finalImg = removeBackground(croppedImg)
    
            rename = im.split('.')[0] + ".png"
            finalImg.thumbnail((224, 224))
            finalImg.save(DES + rename, optimize=True, quality=95)
