from PIL import Image, ImageFilter
from random import randint
import os

f = r'D:\3rd Year\ELITE_IMAGE_PROCESSING\TEST_IMAGES\White_King_Color_75_1' #Adress to King chess pieces


def removeBlackBgInKing(img):
    img = img.convert("RGB")
    datas = img.getdata()
    new_image_data = []
    for item in datas:
        if item[0] == 0:
            new_image_data.append((255, 255, 255))
        else:
            new_image_data.append(item)
    img.putdata(new_image_data)
    return img

#Different positions to arrange chess pieces
sizesList = [[0,0,224,224],[44.8,0,268.8,224],[33.6,0,257.6,224],[22.4,0,246.4,224],[-22.4,0,201.6,224],[-33.6,0,190.4,224],[-44.8,0,179.2,224],[-67.2,0,156.8,224]]

#Crop and Rotate Code
for file in os.listdir(f):
    im = f + "/" + file
    img = Image.open(im)
    img = img.convert('RGBA')
    img = img.crop(box =(969,364,1680,1055))
    """ angle - 0 - img = img.crop(box = (350,33,1353,1036))
                15 - img = img.crop(box = (406,-2,1464,1056))
                30 - img = img.crop(box = (484,57,1482,1055))
                45 - img = img.crop(box = (528,48,1592,1112))
                60 - img = img.crop(box = (681,219,1505,1043))
                75 - img = img.crop(box = (749,330,1384,965))"""
    img = img.rotate(randint(-15, 15), expand = 1)
    img = removeBlackBgInKing(img)
    img.thumbnail((224,224))
    img.save(im)

#Code for diffrent positions
for file in os.listdir(f):
    im = f + "/" + file
    img = Image.open(im)
    c = sizesList[randint(0,6)]
    img = img.crop(box = (c[0],c[1],c[2],c[3]))
    img = removeBlackBgInKing(img)
    img.save(im)

f = r'D:\3rd Year\ELITE_IMAGE_PROCESSING\TEST_IMAGES\queen\White_King_Color_75_1' #Adress to King chess pieces

#to Compress the file size
for file in os.listdir(f):
    im = f + "/" + file
    img = Image.open(im)
    img.save(im,optimize=True, quality=95)


